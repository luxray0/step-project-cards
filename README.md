# [Step-project 'Cards'](https://sllawwkoo.gitlab.io/step-project-3-cards/)

***
## Команда проекту:

- [Яременко Ян](https://t.me/yan_yaremenko)
    - ## Виконав:
        - [x] Верстка
        - [x] Адаптивна верстка
        - [x] JS (часткова логіка)
        - [x] API
        - [x] Рефакторинг
        - [x] fixed bugs
***
- [Новак Інгрід](https://t.me/obadaya)
    - ## Виконала:
        - [x] Архітектура класів
        - [x] Об'єднала усі JS файли в загальну логіку
        - [x] API
        - [x] Рефакторинг
        - [x] fixed bugs
***

- [Пивоваров Вячеслав](https://t.me/sllawwkoo)
    - ## Виконав:
        - [x] Фільтрація карток
        - [x] Переміщення карток(Drag-and-drop)
        - [x] API
        - [x] Рефакторинг
        - [x] fixed bugs
***

## Технології

- [GulpJS](https://gulpjs.com/)
- [SASS](https://sass-lang.com/), [SCSS]()
- [HTML]()
- [BEM]()
- [JavaScript]()
- [JavaScript ES6 modules]()
- [rest API]()

### Вимоги

Для встановлення та запуску проекту, необхідний [NodeJS](https://nodejs.org/) v8+.

### Встановлення залежностей

Для встановлення залежностей, виконайте команду:

```sh
$ npm i
```

### Запуск Development сервера

Щоб запустити сервер для розробки, виконайте команду:

```sh
$ npm run dev
```

### Створення білда

Щоб виконати production збірку, виконайте команду:

```sh
$ npm run build
```
### Логін та пароль

```sh
login: secretary@gmail.com
password: secretary 
```