export let allModals = {
	"authModal": {
		 class: "AuthModal",
		 tagName: "div",
		 attributes: {
			  id: "form__login",
		 },
		 classNames: "modal",
	},
	"createVisitModal": {
		 class: "CreateVisitModal",
		 tagName: "div",
		 attributes: {
			  id: "create_visit_form",
		 },
		 classNames: "modal",
	},
}

