export function showLoginFormMob() {
    const loginBtn = document.querySelector('.login__button');

    document.getElementById("form__email").defaultValue = "secretary@gmail.com";
    document.getElementById("form__password").defaultValue = "secretary";
    if (window.matchMedia("(max-width: 768px)").matches) {
        loginBtn.addEventListener('click', (e) => {
            e.preventDefault()

            const img = document.querySelector('.login__form-img')

            img.style.display = 'none'
        })
    }
}