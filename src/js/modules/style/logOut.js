export function toogleExid(btn, elem) {
    const avatar = document.querySelector(btn)

    avatar.addEventListener('click', () => {

        const showWindow = document.querySelector(elem)
        showWindow.classList.toggle('show')
    })
}

export function clearPage() {
    const logOutBtn = document.querySelector('.logOut__button')

    logOutBtn.addEventListener('click', () => {
        localStorage.clear();
        window.location.reload();

    })
}