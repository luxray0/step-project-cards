export function showFilter() {
    const filterBtn = document.querySelector('.filter__box');
    if (window.matchMedia("(max-width: 992px)").matches) {

        filterBtn.addEventListener('click', (e) => {
            e.preventDefault()

            const filterMenu = document.querySelector('.main__content-left')

            const bg = document.querySelector('.bg')
            bg.style.display = 'flex'

            filterMenu.classList.toggle('toggle__queries')
        })
    }

}

export function closeFilter(btn, element) {
    const filterBtn = document.querySelector(btn);
    if (window.matchMedia("(max-width: 992px)").matches) {

        filterBtn.addEventListener('click', (e) => {
            e.preventDefault()

            const filterMenu = document.querySelector(element)

            const bg = document.querySelector('.bg')
            bg.style.display = 'none'

            filterMenu.classList.add('toggle__queries')
        })
    }
}