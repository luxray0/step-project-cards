export class AuthAPIService {
    email
    password

    constructor() {
        this.email = document.getElementById('form__email')
        this.password = document.getElementById('form__password')
    }

    async sendAuthRequest(e) {
        const response = await fetch("https://ajax.test-danit.com/api/v2/cards/login", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ email: this.email.value, password: this.password.value })
        }).catch(err => console.log(err))
        if (await response.status === 200) {
            localStorage.setItem("token", await response.text())
            localStorage.setItem('email', this.email.value)

            e.preventDefault()
            e.target.style.display = 'none'
            document.querySelector('.login__form-btn').style.display = 'none'
            const loader = document.querySelector('.loader')
            loader.style.display = 'flex'

            setTimeout(() => {
                location.reload()
            }, 1500)
        } else {
            alert(await response.text())
        }
    }

    isLoggedIn() {
        return localStorage.getItem("token") !== null
    }

    tryLogin(func) {
        const logoReload = document.querySelector('.header__logo')
        logoReload.addEventListener('click', () => {
            window.location.reload();
        })
        window.addEventListener('DOMContentLoaded', () => {

            if (localStorage.getItem("token")) {
                const alert = document.querySelector('.alert')
                const alertText = alert.querySelector('.alert__user-email')
                alertText.textContent = localStorage.getItem("email")

                setTimeout(() => {
                    alert.style.display = 'flex'
                }, 2000)

                setTimeout(() => {
                    alert.style.opacity = '1'
                }, 2010)

                setTimeout(() => {
                    alert.style.opacity = '0'
                }, 6000)

                setTimeout(() => {
                    alert.style.display = 'none'
                }, 6700)

                func()
            }
        })
    }
}