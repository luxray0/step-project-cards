import { Modal } from "./Modal.js";

export class ModalAuth extends Modal {
	bind() {
		super.bind();//викликає батьківський bind(спочатку забіндить кнопки закриття модалки)

		document.querySelector(".login__button").onclick = this.open.bind(this);
	}

	open(e) {
		e.preventDefault()

		const loginForm = document.querySelector('.form__login')
		loginForm.style.display = 'flex'
		setTimeout(() => {
			loginForm.style.transform = 'translate(0px)'
		}, 10)
	}

	loadMain() {
		const mainPageLeft = document.querySelector('.main__content-left')
		const mainPageRight = document.querySelector('.main__content-right')
		const loginPage = document.querySelector('.login')

		loginPage.style.display = 'none'
		mainPageLeft.style.display = 'flex'
		mainPageRight.style.display = 'flex'

		const loginBtn = document.querySelector('.login__button')
		const visitBtn = document.querySelector('.visit__button')

		loginBtn.style.display = 'none'
		visitBtn.style.display = 'flex'

		const email = localStorage.getItem('email');
		const headerEmail = document.querySelector('.header__user-email')
		const secretaryAvatar = document.querySelector('.header__user')
		secretaryAvatar.style.display = 'flex'
		headerEmail.textContent = email
	}
}
