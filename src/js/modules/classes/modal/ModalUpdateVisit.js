import { ModalCreateVisit } from "./ModalCreateVisit.js";

export class ModalUpdateVisit extends ModalCreateVisit {
	id
	fullname
	selectedDoctor
	purpose
	description
	priority
	status
	age
	normalPressure
	bodyMassIndex
	cardiovascularDiseases
	lastVisit

	constructor(params) {
		super(params);

		this.id = params.id
		this.fullname = params.fullName
		this.selectedDoctor = params.doctor
		this.purpose = params.purpose
		this.description = params.description
		this.priority = params.priority
		this.status = params.status
		this.age = params.age
		this.normalPressure = params.normalPressure
		this.bodyMassIndex = params.bodyMassIndex
		this.cardiovascularDiseases = params.cardiovascularDiseases
		this.lastVisit = params.lastVisitDate
	}
	open() {
		super.open()

		document.querySelector('#doctor__select').disabled = true

		this.doctorSelect.value = this.selectedDoctor

		document.querySelector('.priority-select').value = this.priority;

		document.querySelector(".priority-select").insertAdjacentHTML('afterend', `
					<div class="main__select">
						<select class="custom-select" id="edit-status">
							<option value="open" selected>Відкритий</option>
							<option value="done">Закритий</option>
						</select>
					</div>`);

		document.querySelector('.update-form').style.display = 'block';
		document.querySelector('.send-form').style.display = 'none';

		function removeSelect(btn) {
			btn.addEventListener('click', () => {
				document.querySelector('#edit-status').style.display = 'none'
				document.querySelector('#doctor__select').disabled = false
			})
		}
		removeSelect(document.querySelector('.cancel'))
		removeSelect(document.querySelector('.bg'))
		removeSelect(document.querySelector('.form__header-close'))
		removeSelect(document.querySelector('.send-form'))

		let event = new Event('change');
		this.doctorSelect.dispatchEvent(event);

		document.querySelector('.full__name').value = this.fullname;
		document.querySelector('#visit').value = this.purpose
		document.getElementById('description').value = this.description;



		if (document.getElementById('age') !== undefined && document.getElementById('age') !== null) {
			document.getElementById('age').value = this.age;
		}
		if (document.getElementById('pressure') !== undefined && document.getElementById('pressure') !== null) {
			document.getElementById('pressure').value = this.normalPressure;
		}
		if (document.getElementById('body-mass') !== undefined && document.getElementById('body-mass') !== null) {
			document.getElementById('body-mass').value = this.bodyMassIndex;
		}
		if (document.getElementById('hearth') !== undefined && document.getElementById('hearth') !== null) {
			document.getElementById('hearth').value = this.cardiovascularDiseases;
		}
		if (document.querySelector('#date') !== undefined && document.querySelector('#date') !== null) {
			document.querySelector('#date').value = this.lastVisit;
		}

	}

	createVisit() {
		return super.createVisit(this.id);
	}

}