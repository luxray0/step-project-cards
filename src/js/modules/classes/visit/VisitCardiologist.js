import { Visit } from "./Visit.js";

export class VisitCardiologist extends Visit {
	constructor(id, doctor, purpose, description, priority, fullName, status, normalPressure, bodyMassIndex, cardiovascularDiseases, age) {
		super(id, doctor, purpose, description, priority, fullName, status);
		this.normalPressure = normalPressure;
		this.bodyMassIndex = bodyMassIndex;
		this.cardiovascularDiseases = cardiovascularDiseases;
		this.age = age;
	}

	render(id) {
		let input = `
		 <div class="wrapper-patient" data-wrap="${id}">
			  <div class="patient" data-card="${id}">
					<div class="patient__header">
						 <div class="patient__status status-${this.status}">${this.getStatus()}</div>
						 <h1 class="patient__header-title">Візит <span>№${id}</span></h1>
						 <button class="patient__header-close">close</button>
					</div>
					<div class="patient__content">
						 <h3>Лікар: <span class="card-field patient__doctor">Кардіолог</span></h3>
						 <div class="patient__content-urgency">
							  <h3>Терміновість: ${this.getUrgencyLabel()}</h3>
							  <button type="submit" class="button-urgency btn-${this.priority}"></button>
						 </div>
						 <h3>ПІБ Пацієнта: <span class="card-field patient__fullname">${this.fullName}</span></h3>
						 <div class="hidden__content hidden">
							  <h3>Мета візиту: <span class="card-field hidden__purpose">${this.purpose}</span></h3>
							  <h3>Короткий опис візиту: <span class="card-field hidden__description">${this.description}</span>
							  </h3>
							  <h3>Звичайний тиск: <span class="card-field patient__pressure">${this.normalPressure}</span></h3>
							  <h3>Індекс маси тіла: <span class="card-field patient__weigth">${this.bodyMassIndex}</span></h3>
							  <h3>Перенесені захворювання: <span class="card-field patient__diseases">${this.cardiovascularDiseases}</span>
							  </h3>
							  <h3>Вік: <span class="card-field patient__age">${this.age}</span></h3>
						 </div>
						 <div class="patient__content-buttons">
							  <button class="patient__content-button edit-btn">
									<span class="edit-btn">Редагувати</span>
							  </button>
							  <button class="patient__content-button show__more-button">
									<span>Показати більше</span>
							  </button>
						 </div>
					</div>
			  </div>
		 </div>
		 `
		const parentElement = document.querySelector('.patients-wrapper')
		parentElement.insertAdjacentHTML("afterbegin", input)
	}

	update() {
		const visit = document.querySelector(`[data-card="${this.id}"]`);

		visit.querySelector('.patient__status').innerHTML = this.getNewStatus();
		visit.querySelector('.patient__status').className = `patient__status status-${this.status}`

		visit.querySelector('.patient__doctor').innerHTML = this.doctor;
		visit.querySelector('.patient__fullname').innerHTML = this.fullName;
		visit.querySelector('.hidden__purpose').innerHTML = this.purpose;
		visit.querySelector('.hidden__description').innerHTML = this.description;
		visit.querySelector('.patient__pressure').innerHTML = this.normalPressure;
		visit.querySelector('.patient__weigth').innerHTML = this.bodyMassIndex;
		visit.querySelector('.patient__diseases').innerHTML = this.cardiovascularDiseases;
		visit.querySelector('.patient__age').innerHTML = this.age;


		visit.querySelector('.patient__content-urgency').children[0].innerHTML = `Терміновість: ${this.getUrgencyLabel()}`;
		visit.querySelector('.patient__content-urgency').children[1].className = `button-urgency btn-${this.priority}`;
	}
}
