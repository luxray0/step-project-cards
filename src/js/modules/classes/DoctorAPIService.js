export class DoctorAPIService {
    constructor() {

    }

    async createCard(card) {
        try {
            const response = await fetch("https://ajax.test-danit.com/api/v2/cards", {
                method: "POST",
                headers: {
                    'Content-Type': "application/json",
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                },
                body: JSON.stringify(card),
            })
            if (!response) {
                throw new Error("Некоректні введені дані")
            }
            return await response.json()
        } catch (err) {
            console.error('Error >>', err)
        }
    }
    async getAllCards() {
        try {
            const response = await fetch('https://ajax.test-danit.com/api/v2/cards', {
                method: "GET",
                headers: {
                    'Content-Type': "application/json",
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                },
            })
            if (!response) {
                throw new Error("Некоректні введені дані")
            }
            return await response.json()
        } catch (err) {
            console.error('Error >>', err)
        }
    }
    async getCard(id) {
        try {
            const response = await fetch('https://ajax.test-danit.com/api/v2/cards/'+ id, {
                method: "GET",
                headers: {
                    'Content-Type': "application/json",
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                },
            })
            if (!response) {
                throw new Error("Некоректні введені дані")
            }
            return await response.json()
        } catch (err) {
            console.error('Error >>', err)
        }
    }
    async editCard(card) {
        try {
            const response = await fetch(`https://ajax.test-danit.com/api/v2/cards/${card.id}`, {
                method: "PUT",
                headers: {
                    'Content-Type': "application/json",
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                },
                body: JSON.stringify(card),
            })
            if (!response) {
                throw new Error("Некоректні введені дані")
            }
            const data = await response.json()
            return data
        } catch (err) {
            console.error('Error >>', err)
        }
    }
    async deleteCard(cardId) {
        try {
            const response = await fetch(`https://ajax.test-danit.com/api/v2/cards/${cardId}`, {
                method: "DELETE",
                headers: {
                    'Content-Type': "application/json",
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                },
            })
            if (!response) {
                throw new Error("Неможливе виданення")
            }
            return true

        } catch (err) {
            console.error('Error >>', err)
        }
    }
}
